cmake_minimum_required(VERSION 3.14)

set(PROJECT_UNIT_TESTS_NAME ${PROJECT_NAME_STR}_unit_tests)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage -O0 -g -fprofile-arcs -ftest-coverage")

find_package(Boost COMPONENTS filesystem system unit_test_framework REQUIRED)

set(UNIT_TESTS_INCLUDES ${PROJECT_INCLUDE_DIR}
        ${PROJECT_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIR})

set (Main_project_src 
        ../src/model/lobby.cpp 
        ../src/model/player.cpp 
        ../src/model/ingame.cpp 
        ../src/model/request.cpp 
        ../src/model/base_model.cpp)

include_directories(${Boost_INCLUDE_DIRS})
include_directories(${UNIT_TESTS_INCLUDES})
include_directories(../src/model ../src/dao)

add_executable(${PROJECT_UNIT_TESTS_NAME} ${UNIT_TESTS_SRC_FILES} ${Main_project_src} dao_tests.cpp model_tests.cpp)
target_link_libraries(${PROJECT_UNIT_TESTS_NAME} ${Boost_LIBRARIES})

target_compile_features(${PROJECT_UNIT_TESTS_NAME} PRIVATE cxx_std_17)

add_test(UnitTests ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${PROJECT_UNIT_TESTS_NAME})