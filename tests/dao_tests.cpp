#define BOOST_TEST_MODULE SearchResultGameUnitTests

#include <boost/test/unit_test.hpp>
#include "../src/model/base_model.h"
#include "../src/dao/in_memory_dao.h"

#include "mock_obj.h"

BOOST_AUTO_TEST_SUITE(DAO_Tests)

using namespace srg::dao;

BOOST_AUTO_TEST_CASE(Add_Test) {
    InMemoryDAO<MockObj>::instance().clear();
    
    BOOST_CHECK_EQUAL(InMemoryDAO<MockObj>::instance().add(MockObj(0)), true);
    BOOST_CHECK_EQUAL(InMemoryDAO<MockObj>::instance().add(MockObj(0)), false);
}

BOOST_AUTO_TEST_CASE(Get_Test) {
    InMemoryDAO<MockObj>::instance().clear();
    
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(0)));

    auto opt = InMemoryDAO<MockObj>::instance().getByID(0);
    BOOST_ASSERT(opt);
    BOOST_CHECK_EQUAL(opt.value().getID(), 0);
}

BOOST_AUTO_TEST_CASE(GetAll_Test) {
    InMemoryDAO<MockObj>::instance().clear();

    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(0)));
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(1)));

    auto all = InMemoryDAO<MockObj>::instance().getAll();
    std::vector<MockObj> expected = { MockObj(0), MockObj(1) };
    
    for (int i = 0; i < all.size(); i++) {
        BOOST_CHECK_EQUAL(expected[i].getID(), all[i].getID());
    }
}

BOOST_AUTO_TEST_CASE(GetAllWithIDs_Test) {
    InMemoryDAO<MockObj>::instance().clear();
    
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(0)));
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(1)));
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(2)));

    auto all = InMemoryDAO<MockObj>::instance().getAllWithIDs({0, 2});
    std::vector<MockObj> expected = { MockObj(0), MockObj(2) };
    
    for (int i = 0; i < all.size(); i++) {
        BOOST_CHECK_EQUAL(expected[i].getID(), all[i].getID());
    }
}

BOOST_AUTO_TEST_CASE(Update_Test) {
    InMemoryDAO<MockObj>::instance().clear();
    
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(0)));
    
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().getByID(0));
    auto obj = InMemoryDAO<MockObj>::instance().getByID(0).value();
    obj.testField = 1;
    InMemoryDAO<MockObj>::instance().update(0, obj);

    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().getByID(0));
    auto newobj = InMemoryDAO<MockObj>::instance().getByID(0).value();

    BOOST_CHECK_EQUAL(obj.testField, newobj.testField);
}

BOOST_AUTO_TEST_CASE(Remove_Test) {
    InMemoryDAO<MockObj>::instance().clear();
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().add(MockObj(0)));
    BOOST_ASSERT(InMemoryDAO<MockObj>::instance().getByID(0));
    BOOST_CHECK_EQUAL(InMemoryDAO<MockObj>::instance().remove(0), true);
    BOOST_CHECK_EQUAL(InMemoryDAO<MockObj>::instance().getByID(0).has_value(), false);
}

BOOST_AUTO_TEST_SUITE_END()