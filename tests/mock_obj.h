#include "../src/model/base_model.h"

struct MockObj : public srg::model::base_model {
    MockObj(int id): base_model(id) {};
    MockObj(const MockObj& other) = default;
    MockObj(MockObj&& other) = default;

    MockObj& operator=(const MockObj& other) = default;

    int testField = 0;
};