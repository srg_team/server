#include <boost/test/unit_test.hpp>
#include "../src/model/player.h"
#include "../src/model/request.h"
#include "../src/model/lobby.h"
#include "../src/model/ingame.h"

using namespace srg::model;

BOOST_AUTO_TEST_SUITE(Model_Tests)


BOOST_AUTO_TEST_CASE(Player_Test) {
    Player player(0, "aaa");
    BOOST_CHECK_EQUAL(player.getID(), 0);
    BOOST_CHECK_EQUAL(player.getUsername(), "aaa");
    player.setID(1);
    player.setUsername("test");
    BOOST_CHECK_EQUAL(player.getID(), 1);
    BOOST_CHECK_EQUAL(player.getUsername(), "test");
}

BOOST_AUTO_TEST_CASE(Request_Test) {
    Request request("google", 64);
    BOOST_CHECK_EQUAL(request.getID(), 0);
    BOOST_CHECK_EQUAL(request.getRequest(), "google");
    BOOST_CHECK_EQUAL(request.getCount(), 64);
}

BOOST_AUTO_TEST_SUITE(Lobby_Tests)

BOOST_AUTO_TEST_CASE(Add_Players_Test) {
    Lobby lobby;
    BOOST_CHECK_EQUAL(lobby.getID(), 0);
    BOOST_CHECK_EQUAL(lobby.getPlayers().size(), 0);
    lobby.addPlayer(0);
    lobby.addPlayer(1);
    BOOST_CHECK_EQUAL(lobby.getPlayers().size(), 2);

    auto all = lobby.getPlayers();
    std::vector<int> expected = { 0, 1 };
    BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), all.begin(), all.end());
}

BOOST_AUTO_TEST_CASE(State_Test) {
    Lobby lobby;
    BOOST_CHECK_EQUAL(lobby.getState() == Lobby::State::CREATED, true);
    lobby.setStarted();
    BOOST_CHECK_EQUAL(lobby.getState() == Lobby::State::STARTED, true);
    lobby.setFinished();
    BOOST_CHECK_EQUAL(lobby.getState() == Lobby::State::FINISHED, true);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(InGame_Tests)

BOOST_AUTO_TEST_CASE(Getters_Test) {
    InGame game({0, 1}, {0, 1, 2, 3});
    std::vector<int> playersExpected = {0, 1};
    BOOST_CHECK_EQUAL_COLLECTIONS(game.getPlayers().begin(), game.getPlayers().end(),
                                  playersExpected.begin(), playersExpected.end());
    
    std::vector<int> requestsExpected = {0, 1, 2, 3};
    BOOST_CHECK_EQUAL_COLLECTIONS(game.getRequests().begin(), game.getRequests().end(),
                                  requestsExpected.begin(), requestsExpected.end());
    BOOST_CHECK_EQUAL(game.isOver(), false);
    BOOST_CHECK_EQUAL(game.getPickedRequests().size(), 0);
}

BOOST_AUTO_TEST_CASE(Session_Test) {
    InGame game({0, 1}, {0, 1, 2, 3});
    
    BOOST_CHECK_EQUAL(game.pickRequest(0, 10), false);
    
    BOOST_CHECK_EQUAL(game.pickRequest(0, 1), true);

    BOOST_CHECK_EQUAL(game.isOver(), false);
    BOOST_CHECK_EQUAL(game.getPlayerRequest(1), -1);

    BOOST_CHECK_EQUAL(game.pickRequest(0, 1), false);
    BOOST_CHECK_EQUAL(game.pickRequest(0, 2), false);
    BOOST_CHECK_EQUAL(game.pickRequest(1, 1), false);
    BOOST_CHECK_EQUAL(game.pickRequest(1, 2), true);
    BOOST_CHECK_EQUAL(game.pickRequest(1, 3), false);

    BOOST_CHECK_EQUAL(game.pickRequest(10, 3), false);


    BOOST_CHECK_EQUAL(game.isOver(), true);

    BOOST_CHECK_EQUAL(game.getPlayerRequest(0), 1);
    BOOST_CHECK_EQUAL(game.getPlayerRequest(1), 2);
}


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Model_Json_Tests)

BOOST_AUTO_TEST_CASE(Player_Test) {
    Player player(1, "test");
    nlohmann::json json;
    to_json(json, player);
    nlohmann::json expected = { {"id", 1}, {"username", "test"} };
    BOOST_CHECK_EQUAL(json, expected);
    Player other(0, "");
    from_json(json, other);

    BOOST_CHECK_EQUAL(player.getID(), other.getID());
    BOOST_CHECK_EQUAL(player.getUsername(), other.getUsername());
}

BOOST_AUTO_TEST_CASE(Request_Test) {
    Request req("test", 1);
    nlohmann::json json;
    to_json(json, req);
    nlohmann::json expected = { {"id", 1}, {"request", "test"}, {"count", 1} };
    BOOST_CHECK_EQUAL(json, expected);
}

BOOST_AUTO_TEST_CASE(Lobby_Test) {
    Lobby lobby;
    nlohmann::json json;
    to_json(json, lobby);
    nlohmann::json expected = { {"id", 2}, {"state", 0}, {"players", std::vector<int>()} };
    BOOST_CHECK_EQUAL(json, expected);
}

BOOST_AUTO_TEST_CASE(Ingame_Test) {
    InGame game({}, {});
    nlohmann::json json;
    to_json(json, game);
    nlohmann::json expected = { {"id", 2}, {"players", std::vector<int>()}, {"requests", std::vector<int>()} };
    BOOST_CHECK_EQUAL(json, expected);
}

BOOST_AUTO_TEST_SUITE_END()
