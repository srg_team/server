#include "request.h"


namespace srg {
namespace model {

Request::Request(const std::string& req, uint64_t cnt):
        base_model(requestSeq),
        request(req),
        count(cnt) {
    requestSeq++;
}

const std::string& Request::getRequest() const {
    return request;
}

const uint64_t Request::getCount()   const {
    return count;
}

void to_json(nlohmann::json& json, const Request& request) {
    base_to_json(json, request);
    json.update(nlohmann::json {
        {"request", request.getRequest()},
        {"count", request.getCount()}
    });
}

}
}
