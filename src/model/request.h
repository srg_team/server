#pragma once

#include <string>
#include <iostream>

#include "base_model.h"

namespace srg {
namespace model {

class Request: public base_model {
public:
    explicit Request(const std::string& req, uint64_t cnt);
    Request(const Request& other) = default;
    Request(Request&& other) = default;

    const std::string& getRequest() const;
    const uint64_t     getCount()   const;

    Request& operator=(const Request& other) = default;

private:
    std::string request;
    uint64_t count;

    static inline int requestSeq = 0;
};

void to_json(nlohmann::json& json, const Request& request);
}
}
