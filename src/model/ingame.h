#pragma once

#include "base_model.h"
#include <algorithm>

namespace srg {
namespace model {

class InGame: public base_model {
public:
    explicit InGame(const std::vector<int>& plrs, const std::vector<int>& rqsts);
    InGame(const InGame& other) = default;
    InGame(InGame&& other) = default;

    const std::vector<int>& getPlayers()  const;
    const std::vector<int>& getRequests() const;

    const std::vector<std::pair<int, int>>& getPickedRequests() const;
    const int getPlayerRequest(int player) const;

    bool isOver() const;
    
    bool pickRequest(int player, int request);

    InGame& operator=(const InGame& other) = default;

private:
    std::vector<int> players;
    std::vector<int> requests;
    std::vector<std::pair<int, int>> pickedRequests;

    static inline int ingameSeq = 0;
};

void to_json(nlohmann::json& json, const InGame& inGame);

}
}
