#include "lobby.h"

namespace srg {
namespace model {

Lobby::Lobby(): base_model(lobbySeq), state(State::CREATED) {
    lobbySeq++;
}

const std::vector<int> Lobby::getPlayers() const {
    std::vector<int> res;
    res.reserve(players.size());
    std::transform(players.begin(), players.end(),
        std::back_inserter(res), [](const auto& pair) {
            return pair.first;
        });

    return res;
}

const Lobby::State Lobby::getState() const {
    return state;
}

const bool Lobby::isPlayerReady(const int id) const {
    if (players.count(id) == 0) {
        return false;
    }
    return players.at(id);
}

void Lobby::addPlayer(const int id) {
    players.insert(std::make_pair(id, false));
}

void Lobby::setPlayerReady(const int id) {
    if (players.count(id)) {
        players[id] = true;
    }
    int count = std::count_if(players.begin(), players.end(),
        [](const auto& p) {
            return p.second == true;
        });
    if (count == players.size()) {
        setStarted();
    }
}

void Lobby::setStarted() {
    state = State::STARTED;
}

void Lobby::setFinished() {
    state = State::FINISHED;
}

void to_json(nlohmann::json& json, const Lobby& lobby) {
    base_to_json(json, lobby);

    nlohmann::json players = dao::InMemoryDAO<Player>::instance().getAllWithIDs(lobby.getPlayers());
    for (auto& player: players) {
        player.update( {
            {"isReady", lobby.isPlayerReady(player["id"].get<int>()) }
        });
    }

    json.update(nlohmann::json {
        {"state", lobby.getState()},
        {"players", players}
    });
}

}
}
