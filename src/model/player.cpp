#include "player.h"

namespace srg {
namespace model {


Player::Player(const std::string& name) : base_model(playerSeq), username(name) {
    playerSeq++;
};

Player::Player(int _id, const std::string& name) : base_model(_id), username(name) {
};

const std::string& Player::getUsername() const {
    return username;
}

void Player::setUsername(const std::string& name) {
    username = name;
}

const int Player::getID() const {
    return id;
}

void to_json(nlohmann::json& json, const Player& player) {
    base_to_json(json, player);
    json.update( nlohmann::json{{"username", player.getUsername()}} );
}

void from_json(const nlohmann::json& json, Player& player) {
    base_from_json(json, player);
    player.setUsername(json.at("username").get<std::string>());
}

}
}
