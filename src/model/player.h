#pragma once
#include "base_model.h"

namespace srg {
namespace model {

class Player: public base_model {
public:

    explicit Player(const std::string& username);
    Player(int id, const std::string& username);
    Player(const Player& other) = default;
    Player(Player&& other) = default;

    const std::string& getUsername() const;
    void setUsername(const std::string& name);

    const int getID() const;

    Player& operator=(const Player& other) = default;

    typedef int idType;
private:
    std::string username;

    static inline int playerSeq = 0;
};

void to_json(nlohmann::json& json, const Player& player);
void from_json(const nlohmann::json& json, Player& player);

}
}
