#pragma once
#include "player.h"

#include "base_model.h"
#include "../dao/in_memory_dao.h"

namespace srg {
namespace model {

class Lobby : public base_model {
public:
    enum class State {
        CREATED,
        STARTED,
        FINISHED
    };

public:
    Lobby();
    Lobby(const Lobby& other) = default;
    Lobby(Lobby&& other) = default;

    const std::vector<int>  getPlayers() const;
    const State             getState()   const;
    const bool              isPlayerReady(const int id) const;

    void addPlayer(const int id);
    void setPlayerReady(const int id);
    void setStarted();
    void setFinished();

    Lobby& operator=(const Lobby& other) = default;

private:
    std::map<int, bool> players;
    State state;

    static inline int lobbySeq = 0;
};

void to_json(nlohmann::json& json, const Lobby& lobby);

}
}
