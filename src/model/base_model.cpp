#include "base_model.h"


namespace srg {
namespace model {

void base_to_json(nlohmann::json& json, const base_model& model) {
    json = nlohmann::json {
        {"id", model.getID()}
    };
}

void base_from_json(const nlohmann::json& json, base_model& model) {
    model.setID(json.at("id").get<int>());
}

}
}
