#pragma once

#include <nlohmann/json.hpp>

namespace srg {
namespace model {

template<class T>
struct Message {
    Message() = default;
    explicit Message(const std::string& _type, const T& _data): type(_type), data(_data) {};
    Message(const Message& other) = default;
    Message(Message&& other) = default;

    std::string type;
    T data;
};

template<class T>
void to_json(nlohmann::json& json, const Message<T>& msg) {
    json = {
        {"type", msg.type},
        {"data", msg.data}
    };
};

template<class T>
void from_json(const nlohmann::json& json, Message<T>& msg) {
    msg.type = json.at("type").get<std::string>();
    msg.data = json.at("data").get<T>();
};

}
}
