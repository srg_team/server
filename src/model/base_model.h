#pragma once


#include <nlohmann/json.hpp>

namespace srg {
namespace model {

struct base_model {

    const int getID() const { return id; };
    void setID(const int _id) { id = _id; }

    bool operator<(const base_model& other) { return this->id < other.id; };

    virtual ~base_model() = default;

protected:
    base_model() {};
    base_model(int _id): id(_id) {};

    int id;
};

void base_to_json(nlohmann::json& json, const base_model& model);
void base_from_json(const nlohmann::json& json, base_model& player);

}
}
