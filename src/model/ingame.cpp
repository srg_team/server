#include "ingame.h"

#include "in_memory_dao.h"
#include "player.h"
#include "request.h"

namespace srg {
namespace model {

InGame::InGame(const std::vector<int>& players, const std::vector<int>& requests) :
        base_model(ingameSeq),
        players(players),
        requests(requests) {
    ingameSeq++;
}

const std::vector<int>& InGame::getPlayers()  const {
    return players;
};
const std::vector<int>& InGame::getRequests() const {
    return requests;
};

const std::vector<std::pair<int, int>>& InGame::getPickedRequests() const {
    return pickedRequests;
};
const int InGame::getPlayerRequest(int player) const {
    auto res = std::find_if(pickedRequests.begin(), pickedRequests.end(),
        [player](std::pair<int, int> pair) {
            return pair.first == player;
        }
    );
    if (res == pickedRequests.end()) {
        return -1;
    }
    return res->second;
};

bool InGame::isOver() const {
    return pickedRequests.size() == players.size();
};

bool InGame::pickRequest(int player, int request) {
    if (std::find(players.begin(), players.end(), player) == players.end() ||
            std::find(requests.begin(), requests.end(), request) == requests.end()) {
                return false;
            }

    auto res = std::find_if(pickedRequests.begin(), pickedRequests.end(),
        [player, request](std::pair<int, int> pair) {
            return pair.first == player || pair.second == request;
    });
    if (res != pickedRequests.end()) {
        return false;
    }

    pickedRequests.emplace_back(player, request);

    return true;
}

void to_json(nlohmann::json& json, const InGame& inGame) {
    base_to_json(json, inGame);
    auto reqids = inGame.getRequests();
    nlohmann::json requests = dao::InMemoryDAO<Request>::instance().getAllWithIDs(reqids);
    for (auto& request: requests) {
        request.erase("count");
        request["isPicked"] = false;
    }

    for (const auto& pair: inGame.getPickedRequests()) {
        for (auto& request: requests) {
            if (request.at("id").get<int>() == pair.second) {
                request["isPicked"] = true;
                break;
            }
        }
    }
    json.update( nlohmann::json {
        {"players", dao::InMemoryDAO<Player>::instance().getAllWithIDs(inGame.getPlayers())},
        {"requests", requests}
    });
}


}
}
