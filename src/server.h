#pragma once

#include <ixwebsocket/IXWebSocketServer.h>
#include "model/player.h"
namespace srg {

namespace handlers {

class BaseHandler;

}

class Server {
public:
    explicit Server(int port, const std::string& hostname = "0.0.0.0");
    void init();
    bool startAndWait();

    std::vector<int> getConnectedPlayers() const;
    std::shared_ptr<ix::WebSocket> getPlayerSocket(model::Player::idType id) const;

private:
    void connectionCallback(std::shared_ptr<ix::WebSocket> webSocket,
              std::shared_ptr<ix::ConnectionState> connectionState);
    void onOpen(std::shared_ptr<ix::WebSocket> webSocket);
    void onMessage(std::shared_ptr<ix::WebSocket> webSocket,
              const ix::WebSocketMessagePtr msg);
    void onClose(std::shared_ptr<ix::WebSocket> webSocket);
    void onLogin(std::shared_ptr<ix::WebSocket> webSocket,
              const nlohmann::json& json);

    int getPlayerID(std::shared_ptr<ix::WebSocket> webSocket);

    void handleError(const std::string& desc, std::shared_ptr<ix::WebSocket> webSocket);
private:
    ix::WebSocketServer server;
    std::map<model::Player::idType, std::shared_ptr<ix::WebSocket>> connections;
    std::map<std::string, std::shared_ptr<handlers::BaseHandler>> messageHandlers;
};

}
