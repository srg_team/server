#include "mock_server.h"
#include "../dao/in_memory_dao.h"
#include "../model/player.h"
#include "../model/lobby.h"
#include "../model/ingame.h"
#include "../model/request.h"
#include "../model/message.h"

namespace srg {
namespace mock {

MockServer::MockServer(int port) : server(port, "0.0.0.0") {

}

void MockServer::init() {
    initMockData();
    using namespace std::placeholders;
    server.setOnConnectionCallback(std::bind(&MockServer::connectionCallback, this, _1, _2));
}

bool MockServer::startAndWait() {
    auto res = server.listen();
    if (!res.first)
    {
        std::cerr << res.second << std::endl;
        return false;
    }

    server.start();
    server.wait();
}

void MockServer::connectionCallback(std::shared_ptr<ix::WebSocket> webSocket,
              std::shared_ptr<ix::ConnectionState> connectionState) {
    webSocket->setOnMessageCallback(
                [webSocket, connectionState, this](const ix::WebSocketMessagePtr msg)
                {
                    if (msg->type == ix::WebSocketMessageType::Open) { 
                        this->onOpen(webSocket, msg);
                    }
                    else if (msg->type == ix::WebSocketMessageType::Message) {
                        this->onMessage(webSocket, msg);
                    }
                }
            );

}

void MockServer::onOpen(std::shared_ptr<ix::WebSocket> webSocket,
        const ix::WebSocketMessagePtr msg) {
    nlohmann::json json;
    json = model::Message("lobbies", dao::InMemoryDAO<model::Lobby>::instance().getAll());
    webSocket->send(json.dump(4), msg->binary);
}

void MockServer::onMessage(std::shared_ptr<ix::WebSocket> webSocket,
        const ix::WebSocketMessagePtr msg) {

    using namespace model;
    using namespace dao;
    nlohmann::json json;
    if (nlohmann::json::accept(msg->str)) {
        json = nlohmann::json::parse(msg->str);
        if (!json.contains("type")) {
            json["type"] = "error";
            json["data"] = "Request JSON should have \"type\" field";
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        std::string type = json.at("type").get<std::string>();
        if (type == "me") {
            json = Message("myuserinfo", InMemoryDAO<Player>::instance().getByID(5).value());
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else if (type == "lobbies") {
            json = Message("lobbies", InMemoryDAO<Lobby>::instance().getAll());
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else if (type == "enterlobby") {
            json = Message("mylobby", InMemoryDAO<Lobby>::instance().getByID(0).value());
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else if (type == "ready") {
            json = Message("gamesession", InMemoryDAO<InGame>::instance().getByID(1).value());
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else if (type == "players") {
            json = Message("players", InMemoryDAO<Player>::instance().getAll());
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else if (type == "pick") {
            auto game = InMemoryDAO<InGame>::instance().getByID(1).value();
            nlohmann::json array = nlohmann::json::array();
            for (auto it: game.getPickedRequests()) {
                auto req = InMemoryDAO<Request>::instance().getByID(it.second).value();
                auto player = InMemoryDAO<Player>::instance().getByID(it.first).value();
                array.push_back( {
                    {"username", player.getUsername()},
                    {"request", req.getRequest()},
                    {"score", req.getCount()}
                });
            }
            json = Message("endgame", array);
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
        else {
            json = Message<std::string>("error", "No such message request type");
            webSocket->send(json.dump(4), msg->binary);
            return;
        }
    }
    if (msg->str == "/stop") {
        server.stop();
        return;
    }
    else if (msg->str == "/players") {
        json = InMemoryDAO<Player>::instance().getAll();
        webSocket->send(json.dump(4), msg->binary);
        return;
    }
    else if (msg->str == "/me") {
        json = InMemoryDAO<Player>::instance().getByID(5).value();
        webSocket->send(json.dump(4), msg->binary);
        return;
    }
    else if (msg->str == "/lobby") {
        json = InMemoryDAO<Lobby>::instance().getByID(0).value();
        webSocket->send(json.dump(4), msg->binary);
        return;
    }
    else if (msg->str == "/game") {
        json = InMemoryDAO<InGame>::instance().getByID(0).value();
        webSocket->send(json.dump(4), msg->binary);
        return;
    }
    else if (msg->str == "/lobbies") {
        json = InMemoryDAO<Lobby>::instance().getAll();
        webSocket->send(json.dump(4), msg->binary);
        return;
    }
    json["msg"] = msg->str;
    webSocket->send(json.dump(4), msg->binary);
}

void MockServer::initMockData() {
    using namespace model;
    using namespace dao;

    InMemoryDAO<Player>::instance().add(Player(0, "First Player"));
    InMemoryDAO<Player>::instance().add(Player(1, "Andrew"));
    InMemoryDAO<Player>::instance().add(Player(2, "SimpleUser"));
    InMemoryDAO<Player>::instance().add(Player(3, "BestUser"));
    InMemoryDAO<Player>::instance().add(Player(4, "Totem220"));
    InMemoryDAO<Player>::instance().add(Player(5, "haHAA"));
    InMemoryDAO<Player>::instance().add(Player(6, "Berack"));

    Lobby lobby;
    lobby.addPlayer(3);
    lobby.addPlayer(5);
    lobby.setStarted();
    InMemoryDAO<Lobby>::instance().add(lobby);

    lobby = Lobby();
    lobby.addPlayer(0);
    lobby.addPlayer(6);
    lobby.setFinished();
    InMemoryDAO<Lobby>::instance().add(lobby);

    lobby = Lobby();
    lobby.addPlayer(2);
    InMemoryDAO<Lobby>::instance().add(lobby);

    InMemoryDAO<Request>::instance().add(Request("Google", 25'270'000'000));
    InMemoryDAO<Request>::instance().add(Request("Test", 10'500'000'000));
    InMemoryDAO<Request>::instance().add(Request("Code", 13'140'000'000));
    InMemoryDAO<Request>::instance().add(Request("Cat", 7'200'000'000));
    InMemoryDAO<Request>::instance().add(Request("VK", 934'000'000));
    InMemoryDAO<Request>::instance().add(Request("MailRu", 1'150'000'000));
    InMemoryDAO<Request>::instance().add(Request("Yandex", 604'000'000));

    std::vector<int> players = {3, 5};
    std::vector<int> requests = {0, 1, 2, 3, 4, 5};

    InGame game(players, requests);
    game.pickRequest(3, 1);

    InMemoryDAO<InGame>::instance().add(game);

    players = {0, 6};
    requests = {2, 3, 4, 5, 6, 7};
    game = InGame(players, requests);
    game.pickRequest(0, 5);
    game.pickRequest(6, 6);

    InMemoryDAO<InGame>::instance().add(game);

}

}
}
