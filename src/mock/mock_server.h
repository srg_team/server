#pragma once

#include <ixwebsocket/IXWebSocketServer.h>

namespace srg {
namespace mock {

class MockServer {
public:
    explicit MockServer(int port);
    void init();
    bool startAndWait();

private:
    void connectionCallback(std::shared_ptr<ix::WebSocket> webSocket,
              std::shared_ptr<ix::ConnectionState> connectionState);
    void onOpen(std::shared_ptr<ix::WebSocket> webSocket,
              const ix::WebSocketMessagePtr msg);
    void onMessage(std::shared_ptr<ix::WebSocket> webSocket,
              const ix::WebSocketMessagePtr msg);
    void initMockData();

private:
    ix::WebSocketServer server;
};

}
}
