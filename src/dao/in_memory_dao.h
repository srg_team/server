#pragma once

#include "base_dao.h"

namespace srg {
namespace dao {

template<class T>
class InMemoryDAO : public DAO<T> {
public:
    static DAO<T>& instance() {
        static InMemoryDAO<T> instance;
        return instance;
    }

    using uuid = boost::uuids::uuid;

    virtual std::vector<T>  getAll()                                   const override;
    virtual std::vector<T>  getAllWithIDs(const std::vector<int>& ids) const override;
    virtual std::optional<T> getByID(const int id)                     const override;
    
    virtual std::vector<T> findBy(std::function<bool(const T&)> pred)  const override;
    

    virtual bool add(const T&)            override;
    virtual bool update(const int id, T&) override;
    virtual bool remove(const int id)     override;

    virtual void clear() override;

private:

    std::vector<T> data;
};

template<class T>
std::vector<T> InMemoryDAO<T>::getAll() const {
    return data;
}

template<class T>
std::vector<T> InMemoryDAO<T>::getAllWithIDs(const std::vector<int>& ids) const {
    std::vector<T> result;
    for (const auto& id: ids) {
        std::optional<T> opt = getByID(id);
        if (opt) {
            result.push_back(opt.value());
        }
    }
    return result;
}

template<class T>
std::optional<T> InMemoryDAO<T>::getByID(const int id) const {
    auto res = std::find_if(data.begin(), data.end(), [&id](const model::base_model& model) {
        return model.getID() == id;
    });
    return res != data.end() ? std::make_optional(*res) : std::nullopt;
}

template<class T>
bool InMemoryDAO<T>::update(const int id, T& obj) {
    auto res = std::find_if(data.begin(), data.end(), [&id](const model::base_model& model) {
        return model.getID() == id;
    });
    if (res != data.end()) {
        auto it = data.erase(res);
        data.insert(it, obj);
        return true;
    } else {
        return false;
    }
}

template<class T>
bool InMemoryDAO<T>::add(const T& obj) {
    auto res = std::find_if(data.begin(), data.end(),
        [id = obj.getID()](const model::base_model& model) {
            return model.getID() == id;
        }
    );
    if (res != data.end()) {
        return false;
    }
    data.push_back(obj);
    return true;
}

template<class T>
bool InMemoryDAO<T>::remove(const int id) {
    auto res = std::find_if(data.begin(), data.end(),
        [&id](const model::base_model& model) {
            return model.getID() == id;
        }
    );
    if (res == data.end()) {
        return false;
    }
    data.erase(res);
    return true;
}

template<class T>
void InMemoryDAO<T>::clear() {
    data.clear();
}

template<class T>
std::vector<T> InMemoryDAO<T>::findBy(std::function<bool(const T&)> pred) const {
    std::vector<T> res;
    std::copy_if(data.begin(), data.end(), std::back_inserter(res), pred);
    return res;
}

}
}
