#pragma once

#include <set>
#include <boost/uuid/uuid.hpp>

#include "../model/base_model.h"

namespace srg {
namespace dao {

template<class T, typename = std::enable_if_t<std::is_base_of<model::base_model, T>::value>>
struct DAO {

    virtual std::vector<T>  getAll()                                   const = 0;
    virtual std::vector<T>  getAllWithIDs(const std::vector<int>& ids) const = 0;
    virtual std::optional<T> getByID(const int id)                     const = 0;

    virtual std::vector<T> findBy(std::function<bool(const T&)> pred)  const = 0;

    virtual bool add(const T&) = 0;
    virtual bool update(const int id, T&) = 0;
    virtual bool remove(const int id) = 0;

    virtual void clear() = 0;
};

}
}
