#pragma once
#include <nlohmann/json.hpp>
#include "../model/player.h"

namespace srg {

class Server;

namespace handlers {

class BaseHandler {
public:
    virtual void handleMessage(const srg::model::Player::idType id,
        const nlohmann::json& json) = 0;

    virtual ~BaseHandler() = default;
protected:
    BaseHandler(const Server& _server): server(_server) {};
protected:
    const Server& server;
};

}
}
