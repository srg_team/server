#include "lobby.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/lobby.h"
#include "../model/message.h"

namespace srg {
namespace handlers {

LobbyHandler::LobbyHandler(const Server& _server): BaseHandler(_server) {

}

void LobbyHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    auto& dao = dao::InMemoryDAO<model::Lobby>::instance();

    nlohmann::json answer;
    auto lobbies = dao.findBy([id](const model::Lobby& lobby) {
        auto players = lobby.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && lobby.getState() == model::Lobby::State::CREATED;
    });
    if (lobbies.empty()) {
        answer["type"] = "error";
        answer["data"] = "You haven't active lobby";
        socket->send(answer.dump(4));
        return;
    }

    answer = model::Message("lobby", lobbies[0]);
    socket->send(answer.dump(4));
}

}
}
