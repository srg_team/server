#include "game.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/ingame.h"
#include "../model/message.h"

namespace srg {
namespace handlers {

GameHandler::GameHandler(const Server& _server): BaseHandler(_server) {

}

void GameHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    auto& dao = dao::InMemoryDAO<model::InGame>::instance();

    nlohmann::json answer;
    auto games = dao.findBy([id](const model::InGame& game) {
        auto players = game.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && !game.isOver();
    });
    if (games.empty()) {
        answer["type"] = "error";
        answer["data"] = "You haven't active game";
        socket->send(answer.dump(4));
        return;
    }

    answer = model::Message("game", games[0]);
    socket->send(answer.dump(4));
}

}
}
