#include "ready.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/lobby.h"
#include "../model/message.h"
#include "../model/ingame.h"

namespace srg {
namespace handlers {

ReadyHandler::ReadyHandler(const Server& _server): BaseHandler(_server) {

}

void ReadyHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer;
    auto& dao = dao::InMemoryDAO<model::Lobby>::instance();

    auto lobbies = dao.findBy([id](const model::Lobby& lobby) {
        auto players = lobby.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && lobby.getState() == model::Lobby::State::CREATED;
    });
    if (lobbies.empty()) {
        answer["type"] = "error";
        answer["data"] = "You haven't active lobby";
        socket->send(answer.dump(4));
        return;
    }

    auto lobby = lobbies[0];
    lobby.setPlayerReady(id);
    dao.update(lobby.getID(), lobby);
    if (lobby.getState() != model::Lobby::State::STARTED) {
        answer = model::Message("lobby", lobby);
    } else {
        model::InGame game(lobby.getPlayers(), {0, 1, 2, 3, 4, 5});
        dao::InMemoryDAO<model::InGame>::instance().add(game);
        answer = model::Message("game", game);
    }
    for(const auto playerid: lobby.getPlayers()) {
        server.getPlayerSocket(playerid)->send(answer.dump(4));
    }

}

}
}
