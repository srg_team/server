#include "lobbies.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/lobby.h"
#include "../model/message.h"

namespace srg {
namespace handlers {

LobbiesHandler::LobbiesHandler(const Server& _server): BaseHandler(_server) {

}

void LobbiesHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer = model::Message("lobbies", dao::InMemoryDAO<model::Lobby>::instance().getAll());
    socket->send(answer.dump(4));
}

}
}
