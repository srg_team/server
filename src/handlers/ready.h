#pragma once
#include "base_handler.h"

namespace srg {
namespace handlers {

class ReadyHandler: public BaseHandler {
public:
    ReadyHandler(const Server& server);

    virtual void handleMessage(const srg::model::Player::idType id,
        const nlohmann::json& json) override;
};

}
}
