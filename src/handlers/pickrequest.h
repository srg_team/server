#pragma once
#include "base_handler.h"

namespace srg {
namespace handlers {

class PickRequestHandler: public BaseHandler {
public:
    PickRequestHandler(const Server& server);

    virtual void handleMessage(const srg::model::Player::idType id, const nlohmann::json& json) override;
};

}
}
