#include "pickrequest.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/message.h"
#include "../model/ingame.h"
#include "../model/request.h"

namespace srg {
namespace handlers {

PickRequestHandler::PickRequestHandler(const Server& _server): BaseHandler(_server) {

}

void PickRequestHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer;
    auto& dao = dao::InMemoryDAO<model::InGame>::instance();


    if (!json.contains("requestid") || !json["requestid"].is_number_integer()) {
        answer["type"] = "error";
        answer["data"] = "PickRequest JSON request should contain \"requestid\" field in data";
        socket->send(answer.dump(4));
        return;
    }
    int requestid = json["requestid"].get<int>();

    auto games = dao.findBy([id](const model::InGame& game) {
        auto players = game.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && !game.isOver();
    });
    if (games.empty()) {
        answer["type"] = "error";
        answer["data"] = "You haven't active game";
        socket->send(answer.dump(4));
        return;
    }
    
    auto game = games[0];
    
    if(!game.pickRequest(id, requestid)) {
        answer["type"] = "error";
        answer["data"] = "Failed to pick request";
        socket->send(answer.dump(4));
        return;
    }

    dao.update(game.getID(), game);

    if (!game.isOver()) {
        answer = model::Message("game", game);
    } else {
        nlohmann::json array = nlohmann::json::array();
        for (auto it: game.getPickedRequests()) {
            auto req = dao::InMemoryDAO<model::Request>::instance().getByID(it.second).value();
            array.push_back( { 
                {"username", dao::InMemoryDAO<model::Player>::instance().getByID(it.first).value().getUsername()},
                {"request", req.getRequest()},
                {"score", req.getCount()}
            });
        }
        answer = model::Message("endgame", array);
    }
    for(const auto playerid: game.getPlayers()) {
        server.getPlayerSocket(playerid)-> send(answer.dump(4));
    }

}

}
}
