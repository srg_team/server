#include "joinlobby.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/lobby.h"
#include "../model/message.h"
#include "../model/ingame.h"

namespace srg {
namespace handlers {

JoinLobbyHandler::JoinLobbyHandler(const Server& _server): BaseHandler(_server) {

}

void JoinLobbyHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer;
    auto& dao = dao::InMemoryDAO<model::Lobby>::instance();

    auto lobbies = dao.findBy([id](const model::Lobby& lobby) {
        auto players = lobby.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && lobby.getState() == model::Lobby::State::CREATED;
    });
    if (!lobbies.empty()) {
        answer["type"] = "error";
        answer["data"] = "You already have active lobby";
        socket->send(answer.dump(4));
        return;
    }

    auto& gamedao = dao::InMemoryDAO<model::InGame>::instance();
    auto games = gamedao.findBy([id](const model::InGame& game) {
        auto players = game.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && !game.isOver();
    });
    if (!games.empty()) {
        answer["type"] = "error";
        answer["data"] = "You already have active game";
        socket->send(answer.dump(4));
        return;
    }

    if (!json.contains("lobbyid") || !json["lobbyid"].is_number_integer()) {
        answer["type"] = "error";
        answer["data"] = "JoinLobby JSON request should contain \"lobbyid\" field in data";
        socket->send(answer.dump(4));
        return;
    }

    int lobbyid = json["lobbyid"].get<int>();
    auto opt = dao.getByID(lobbyid);
    if (!opt) {
        answer["type"] = "error";
        answer["data"] = "No lobby with such lobbyid";
        socket->send(answer.dump(4));
        return;
    }

    auto lobby = opt.value();
    if (lobby.getState() != model::Lobby::State::CREATED) {
        answer["type"] = "error";
        answer["data"] = "This lobby already started or finished";
        socket->send(answer.dump(4));
        return;
    }

    if (lobby.getPlayers().size() == 2) {
        answer["type"] = "error";
        answer["data"] = "This lobby is full";
        socket->send(answer.dump(4));
        return;
    }

    lobby.addPlayer(id);
    dao.update(lobbyid, lobby);

    answer = model::Message("lobby", lobby);
    for(const auto playerid: lobby.getPlayers()) {
        server.getPlayerSocket(playerid)-> send(answer.dump(4));
    }
}

}
}
