#include "onlineplayers.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/player.h"
#include "../model/message.h"
#include <iostream>

namespace srg {
namespace handlers {

OnlinePlayersHandler::OnlinePlayersHandler(const Server& _server): BaseHandler(_server) {

}

void OnlinePlayersHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer = model::Message("onlineplayers", 
            dao::InMemoryDAO<model::Player>::instance().getAllWithIDs(server.getConnectedPlayers()));
    socket->send(answer.dump(4));
}

}
}
