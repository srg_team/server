#pragma once
#include "base_handler.h"

namespace srg {
namespace handlers {

class JoinLobbyHandler: public BaseHandler {
public:
    JoinLobbyHandler(const Server& server);

    virtual void handleMessage(const srg::model::Player::idType id,
        const nlohmann::json& json) override;
};

}
}
