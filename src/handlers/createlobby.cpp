#include "createlobby.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/lobby.h"
#include "../model/message.h"
#include "../model/ingame.h"

namespace srg {
namespace handlers {

CreateLobbyHandler::CreateLobbyHandler(const Server& _server): BaseHandler(_server) {

}

void CreateLobbyHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    nlohmann::json answer;

    auto& dao = dao::InMemoryDAO<model::Lobby>::instance();
    auto lobbies = dao.findBy([id](const model::Lobby& lobby) {
        auto players = lobby.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && lobby.getState() == model::Lobby::State::CREATED;
    });
    if (!lobbies.empty()) {
        answer["type"] = "error";
        answer["data"] = "You already have active lobby";
        socket->send(answer.dump(4));
        return;
    }

    auto& gamedao = dao::InMemoryDAO<model::InGame>::instance();
    auto games = gamedao.findBy([id](const model::InGame& game) {
        auto players = game.getPlayers();
        auto it = std::find(players.begin(), players.end(), id);
        return it != players.end() && !game.isOver();
    });
    if (!games.empty()) {
        answer["type"] = "error";
        answer["data"] = "You already have active game";
        socket->send(answer.dump(4));
        return;
    }

    model::Lobby newLobby;
    newLobby.addPlayer(id);
    dao.add(newLobby);
    answer = model::Message("lobby", newLobby);
    socket->send(answer.dump(4));
}

}
}
