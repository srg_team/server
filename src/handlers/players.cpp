#include "players.h"
#include "../server.h"
#include "../dao/in_memory_dao.h"
#include "../model/player.h"
#include "../model/message.h"

namespace srg {
namespace handlers {

PlayersHandler::PlayersHandler(const Server& _server): BaseHandler(_server) {

}

void PlayersHandler::handleMessage(model::Player::idType id, const nlohmann::json& json) {
    auto socket = server.getPlayerSocket(id);
    auto& dao = dao::InMemoryDAO<model::Player>::instance();
    nlohmann::json answer = model::Message("players", dao.getAll());
    socket->send(answer.dump(4));
}

}
}
