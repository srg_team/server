#include <iostream>
#include <nlohmann/json.hpp>
#include <ixwebsocket/IXWebSocketServer.h>
#include "dao/in_memory_dao.h"
#include "model/player.h"
#include "model/lobby.h"
#include "model/ingame.h"
#include "model/request.h"
#include "model/message.h"
#include "server.h"

int main() {
    srg::Server server(9000);
    server.init();

    server.startAndWait();
    return 0;
}
