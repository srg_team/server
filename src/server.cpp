#include "server.h"
#include "dao/in_memory_dao.h"
#include "model/player.h"
#include "model/lobby.h"
#include "model/ingame.h"
#include "model/request.h"
#include "model/message.h"
#include "handlers/base_handler.h"
#include "handlers/lobbies.h"
#include "handlers/players.h"
#include "handlers/onlineplayers.h"
#include "handlers/joinlobby.h"
#include "handlers/pickrequest.h"
#include "handlers/ready.h"
#include "handlers/createlobby.h"
#include "handlers/game.h"
#include "handlers/lobby.h"

namespace srg {

Server::Server(int port, const std::string& hostname) : server(port, hostname) {
    using namespace model;
    using namespace dao;
    InMemoryDAO<Request>::instance().add(Request("Google", 25'270'000'000));
    InMemoryDAO<Request>::instance().add(Request("Test", 10'500'000'000));
    InMemoryDAO<Request>::instance().add(Request("Code", 13'140'000'000));
    InMemoryDAO<Request>::instance().add(Request("Cat", 7'200'000'000));
    InMemoryDAO<Request>::instance().add(Request("VK", 934'000'000));
    InMemoryDAO<Request>::instance().add(Request("MailRu", 1'150'000'000));
    InMemoryDAO<Request>::instance().add(Request("Yandex", 604'000'000));

    messageHandlers["lobbies"] = std::make_shared<handlers::LobbiesHandler>(*this);
    messageHandlers["players"] = std::make_shared<handlers::PlayersHandler>(*this);
    messageHandlers["online"] = std::make_shared<handlers::OnlinePlayersHandler>(*this);
    messageHandlers["joinlobby"] = std::make_shared<handlers::JoinLobbyHandler>(*this);
    messageHandlers["createlobby"] = std::make_shared<handlers::CreateLobbyHandler>(*this);
    messageHandlers["ready"] = std::make_shared<handlers::ReadyHandler>(*this);
    messageHandlers["pickrequest"] = std::make_shared<handlers::PickRequestHandler>(*this);
    messageHandlers["game"] = std::make_shared<handlers::GameHandler>(*this);
    messageHandlers["lobby"] = std::make_shared<handlers::LobbyHandler>(*this);
}

void Server::init() {
    using namespace std::placeholders;
    server.setOnConnectionCallback(std::bind(&Server::connectionCallback, this, _1, _2));
}

bool Server::startAndWait() {
    auto res = server.listen();
    if (!res.first)
    {
        std::cerr << res.second << std::endl;
        return false;
    }

    server.start();
    server.wait();
}

std::shared_ptr<ix::WebSocket> Server::getPlayerSocket(model::Player::idType id) const {
    return connections.at(id);
}

std::vector<int> Server::getConnectedPlayers() const {
    std::vector<int> players;
    players.reserve(connections.size());
    std::transform(connections.begin(), connections.end(),
        std::back_inserter(players), [](const auto& pair) {
            return pair.first;
        });

    return players;
}

void Server::connectionCallback(std::shared_ptr<ix::WebSocket> webSocket,
              std::shared_ptr<ix::ConnectionState> connectionState) {
    webSocket->setOnMessageCallback(
        [webSocket, connectionState, this](const ix::WebSocketMessagePtr msg) {
            if (msg->type == ix::WebSocketMessageType::Open) {
                this->onOpen(webSocket);
            } else if (msg->type == ix::WebSocketMessageType::Message) {
                this->onMessage(webSocket, msg);
            } else if (msg->type == ix::WebSocketMessageType::Close) {
                this->onClose(webSocket);
            }
        });
}

void Server::onOpen(std::shared_ptr<ix::WebSocket> webSocket) {
}

void Server::onMessage(std::shared_ptr<ix::WebSocket> webSocket,
              const ix::WebSocketMessagePtr msg) {
    using namespace model;
    using namespace dao;
    nlohmann::json json;
    if (nlohmann::json::accept(msg->str)) {
        std::cout << "accepted message" << std::endl;
        json = nlohmann::json::parse(msg->str);
        if (!json.contains("type")) {
            handleError("Request JSON should have \"type\" field", webSocket);
            return;
        }
        std::string type = json.at("type").get<std::string>();
        if (type == "login") {
            if (getPlayerID(webSocket) != -1) {
                handleError("You already logged in", webSocket);
                return;
            }
            onLogin(webSocket, json);
            return;
        }
        const auto id = getPlayerID(webSocket);
        if (id == -1) {
            handleError("Please log in", webSocket);
            return;
        }
        auto handler = messageHandlers[type];
        if (!handler) {
            handleError("No such message request type", webSocket);
            return;
        } else {
            handler->handleMessage(id, json["data"]);
        }
    } else {
        handleError("Not JSON request", webSocket);
    }
}

void Server::onClose(std::shared_ptr<ix::WebSocket> webSocket) {
    auto res = std::find_if(connections.begin(), connections.end(),
        [webSocket](const auto& it) {
            return it.second == webSocket;
        });
    if (res != connections.end()) {
        connections.erase(res);
    }
}

int Server::getPlayerID(std::shared_ptr<ix::WebSocket> webSocket) {
    auto res = std::find_if(connections.begin(), connections.end(),
        [webSocket](const auto& it) {
            return it.second == webSocket;
        });
    return res == connections.end() ? -1 : res->first;
}

void Server::onLogin(std::shared_ptr<ix::WebSocket> webSocket,
              const nlohmann::json& json) {
    nlohmann::json answer;
    if (!json.contains("data")) {
        handleError("Request JSON should have \"data\" field", webSocket);
        return;
    }
    nlohmann::json data = json.at("data");
    if (!data.contains("username")) {
        handleError("Data JSON with \"login\" type should have \"username\" field", webSocket);
        return;
    }
    std::string username = data.at("username").get<std::string>();
    const auto& users = dao::InMemoryDAO<model::Player>::instance().findBy(
        [&username](const model::Player other) {
            return other.getUsername() == username;
        }
    );
    if (users.empty()) {
        model::Player newPlayer(username);
        dao::InMemoryDAO<model::Player>::instance().add(newPlayer);
        connections[newPlayer.getID()] = webSocket;
        answer["data"] = newPlayer;
    } else {
        auto player = users[0];
        if (connections.count(player.getID()) > 0) {
            handleError("User with such username on server now", webSocket);
            return;
        } else {
            connections[player.getID()] = webSocket;
        }
        answer["data"] = player;
    }
    answer["type"] = "playerinfo";
    webSocket->send(answer.dump(4));
}

void Server::handleError(const std::string& desc, std::shared_ptr<ix::WebSocket> webSocket) {
    nlohmann::json answer;
    answer["type"] = "error";
    answer["data"] = desc;
    webSocket->send(answer.dump(4));
}

}
