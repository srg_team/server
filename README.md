# Search Result Game

Dependencies (work with vcpkg):

- IXWebSocket
- nlohmann-json
- boost-test
- boost-filesystem
- boost-system

To compile:

```bash
mkdir build
cd build
cmake ..
make
```

To run tests:

```bash
cd build
make test
```

To run:

```bash
cd build/bin
./app
```