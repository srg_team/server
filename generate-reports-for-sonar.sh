#!/bin/sh

cppcheck -v --enable=all --suppress=missingIncludeSystem --xml --check-config -Isrc/dao -Isrc/model src 2> build/cppcheck-report.xml

find src -regex ".*\.cpp\|.*\.h" | vera++ -s -c build/vera-report.xml
sed -i '/T011/d' build/vera-report.xml
sed -i '/T012/d' build/vera-report.xml
sed -i '/T013/d' build/vera-report.xml

./build/bin/SearchResultGame_server_unit_tests --log_format=XML --log_sink=build/tests-report.xml --log_level=all --report_level=no

gcovr -x -r . > build/coverage-report.xml