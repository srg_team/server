# Simple API for mock server

/me - user information about yourself
```json
{
    "id": 0,
    "username": "test"
}
```
/players - online players
```json
[
    {
        "id": 0,
        "username": "test"
    }
]
```
/lobby - your lobby
```json
{
        "id": 0,
        "players": [
            {
                "id": 0,
                "username": "test"
            }
        ],
        "state": 0
    }
```
/lobbies - all lobbies
```json
[
    {
        "id": 0,
        "players": [
            {
                "id": 0,
                "username": "test"
            }
        ],
        "state": 0
    }
]
```
/game - info about your match
```json
{
    "id": 0,
    "players": [
        {
            "id": 0,
            "username": "test"
        }
    ],
    "requests": [
        {
            "id": 0,
            "isPicked": true,
            "request": "test"
        }
    ]
}
```